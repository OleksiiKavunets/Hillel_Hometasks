package Nov23;

import java.util.HashMap;
import java.util.Map;

public class WordCounter {
    public static void main(String[] args){
        String cows = "one one two one two three one two three four one two three four five";
        HashMap<String, Integer> score = wordCount(cows);

        for(Map.Entry<String, Integer> s: score.entrySet()){
            System.out.println(s);
        }
    }

    public static HashMap<String, Integer> wordCount(String s)
    {
        HashMap<String, Integer> wordStat = new HashMap<String,Integer>();

        String[] wordArray = s.split(" ");

        for (String w : wordArray)
        {
            if (wordStat.containsKey(w))
            {
                wordStat.put(w, wordStat.get(w) + 1);

            }
            else
                {
                wordStat.put(w, 1);
            }
        }
        return wordStat;
    }
}
