package Nov23;

public class BinarySearch{

    public static void main(String[] args){

        int[] digits = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        int i1 = searchIndexOf(digits, 7);
        int i2 = searchIndexOf(digits, 3);
        int i3 = searchIndexOf(digits, 6);
        System.out.println("Index of \"7\" is " + i1);
        System.out.println("Index of \"3\" is " + i2);
        System.out.println("Index of \"6\" is " + i3);
    }

    public static int searchIndexOf(int[] a, int key) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {

            int mid = lo + (hi - lo) / 2;
            if      (key < a[mid]) hi = mid - 1;
            else if (key > a[mid]) lo = mid + 1;
            else return mid;
        }
        return -1;
    }
}
