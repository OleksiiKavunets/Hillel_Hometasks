package Nov28;

public class BinaryVerifier
{
    public static boolean isBinary(String number)
    {
        boolean binary = false;
        int num = Integer.valueOf(number);

        while (num > 0)
        {
            int remainder = num % 10;
            if(remainder > 1)
            {
                binary = false;
                break;
            }
            else if (remainder == 0 || remainder == 1)
            {
                binary = true;
            }
            num = num / 10;
        }

        if (binary)
        {
            if(number.charAt(0) == 48) System.out.println(number + " - is binary for sure)");
            else System.out.println(number + " - may be either binary or decimal");
        }
        else System.out.println(number + " - is not binary at all");

        return binary;
    }
}
