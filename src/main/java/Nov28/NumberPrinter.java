package Nov28;

import java.util.ArrayList;
import java.util.List;

public class NumberPrinter {

    private static String y = "##";
    private static String n = "  ";

    public static void printNumber(int number) {
        List<String[][]> numbers = getGivenNumbers(number);

        for (int i = 0; i < numbers.get(0).length; i++)
        {
            for (int j = 0; j < numbers.size(); j++)
            {
                String[][] nums  = numbers.get(j);
                String[] num = nums[i];
                for(String n: num)
                {
                    System.out.print(n);
                }
            }
            System.out.println();
        }
    }

    private static List<String[][]> getGivenNumbers(int number) {
        List<String[][]> numbers = new ArrayList<String[][]>();

        String[] num = String.valueOf(number).split("");

        for(String n: num)
        {
            switch (Integer.valueOf(n)){
                case 0:
                    numbers.add(getZero());
                    break;
                case 1:
                    numbers.add(getOne());
                    break;
                case 2:
                    numbers.add(getTwo());
                    break;
                case 3:
                    numbers.add(getThree());
                    break;
                case 4:
                    numbers.add(getFour());
                    break;
                case 5:
                    numbers.add(getFive());
                    break;
                case 6:
                    numbers.add(getSix());
                    break;
                case 7:
                    numbers.add(getSeven());
                    break;
                case 8:
                    numbers.add(getEight());
                    break;
                case 9:
                    numbers.add(getNine());
                    break;
            }
        }
        return numbers;
    }

    private static String[][] getZero() {
        String[][] arr = new String[][]{
                {n, y, y, y, n, n},
                {y, n, n, n, y, n},
                {y, n, n, n, y, n},
                {y, n, n, n, y, n},
                {n, y, y, y, n, n}
        };
        return arr;
    }

    private static String[][] getOne() {
        String[][] arr = new String[][]{
                {n, y, n, n},
                {y, y, n, n},
                {n, y, n, n},
                {n, y, n, n},
                {y, y, y, n}
        };
        return arr;
    }

    private static String[][] getTwo() {
        String[][] arr = new String[][]{
                {n, y, y, y, n, n},
                {y, n, n, n, y, n},
                {n, n, n, y, n, n},
                {n, n, y, n, n, n},
                {y, y, y, y, y, n}
        };
        return arr;
    }

    private static String[][] getThree() {
        String[][] arr = new String[][]{
                {n, y, y, y, n, n},
                {y, n, n, n, y, n},
                {n, n, n, y, n, n},
                {y, n, n, n, y, n},
                {n, y, y, y, n, n}
        };
        return arr;
    }

    private static String[][] getFour() {
        String[][] arr = new String[][]{
                {n, n, n, y, n, n},
                {n, n, y, y, n, n},
                {n, y, n, y, n, n},
                {y, y, y, y, y, n},
                {n, n, n, y, n, n}
        };
        return arr;
    }

    private static String[][] getFive() {
        String[][] arr = new String[][]{
                {y, y, y, y, n, n},
                {y, n, n, n, n, n},
                {y, y, y, y, n, n},
                {n, n, n, n, y, n},
                {y, y, y, y, n, n}
        };
        return arr;
    }

    private static String[][] getSix() {
        String[][] arr = new String[][]{
                {n, y, y, y, n, n},
                {y, n, n, n, n, n},
                {y, y, y, y, n, n},
                {y, n, n, n, y, n},
                {n, y, y, y, n, n}
        };
        return arr;
    }

    private static String[][] getSeven() {
        String[][] arr = new String[][]{
                {y, y, y, y, y, n},
                {n, n, n, n, y, n},
                {n, n, n, y, n, n},
                {n, n, y, n, n, n},
                {n, y, n, n, n, n}
        };
        return arr;
    }

    private static String[][] getEight() {
        String[][] arr = new String[][]{
                {n, y, y, y, n, n},
                {y, n, n, n, y, n},
                {n, y, y, y, n, n},
                {y, n, n, n, y, n},
                {n, y, y, y, n, n}
        };
        return arr;
    }

    private static String[][] getNine() {
        String[][] arr = new String[][]{
                {n, y, y, y, n, n},
                {y, n, n, n, y, n},
                {n, y, y, y, y, n},
                {n, n, n, n, y, n},
                {n, y, y, y, n, n}
        };
        return arr;
    }
}
