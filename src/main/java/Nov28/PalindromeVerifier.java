package Nov28;

public class PalindromeVerifier {

    public static boolean isPalindrome(int num) {
        int copy = num;
        int reverse = 0;
        while (copy > 0)
        {
            int remainder = copy % 10;
            reverse = reverse * 10 + remainder;
            copy /= 10;
        }
        if(num == reverse)
        {
            System.out.println("Given number " + num + " is palindrome");
        } else {
            System.out.println("Given number " + num + " is not palindrome");
        }
        return num == reverse;
    }
}
