package Nov28;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main28th
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a number you wish to verify to be binary: ");
        String s = bufferedReader.readLine();
        BinaryVerifier.isBinary(s);

        int palindrome = 123454321;
        int notPalindrome = 19283746;
        PalindromeVerifier.isPalindrome(palindrome);
        PalindromeVerifier.isPalindrome(notPalindrome);

        System.out.println("Enter a number you wish to print: ");
        int number = Integer.parseInt(bufferedReader.readLine());
        NumberPrinter.printNumber(number);
    }
}
