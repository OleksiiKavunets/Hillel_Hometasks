package Nov25;

public class TextSorter
{
    public static void sortStringBubble(String  ...x)
    {
        int j;
        boolean flag = true;
        String temp;

        while ( flag )
        {
            flag = false;
            for (j = 0; j < x.length - 1; j++)
            {
                if (x[j].compareToIgnoreCase(x[j + 1]) > 0)
                {
                    temp = x[j];
                    x[j] = x[j + 1];
                    x[j + 1] = temp;
                    flag = true;
                }
            }
        }
        for(String c: x)
        {
            System.out.println(c);
        }
    }
}
