package Nov25;

import java.util.HashMap;
import java.util.Map;

public class Runners
{
    public static HashMap<String, Integer> printBestScores(String[] n, int[] t)
    {
        HashMap<String, Integer> bestMen = new HashMap<String, Integer>();
        int index = 0;
        int first = 0;
        int second = 0;

        for(int i: t)
        {
            if( i > t[first]) first = index;
            else if( i <= t[first] && i >= t[second]) second = index;
            index++;
        }

        bestMen.put(n[first], t[first]);
        bestMen.put(n[second], t[second]);

        for(Map.Entry<String, Integer> m : bestMen.entrySet())
        {
            System.out.println(m);
        }
        return bestMen;
    }
}
