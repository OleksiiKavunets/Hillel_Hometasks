package Nov25;

public class Main25th {

    public static void main(String[] args)
    {
        String[] names = {"John", "Marta", "Vasil", "Petro", "Denys"};
        int[] times = {10, 5, 86, 33, 43};

        Runners.printBestScores(names, times);
        TextSorter.sortStringBubble(names);
    }
}
